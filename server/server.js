const express = require( 'express' );
const app = express();
const fs = require('fs');
const PORT = process.env.PORT || 9999;
const session = require('express-session'); 


app.use( session({ // Express-Session
    secret:'geheim',
    resave:false,

    saveUninitialized:true,
    cookie:{secure:true}
}));

const server = app.listen( PORT, ()=>{
    console.log( 'Server running' );
} );

app.use(express.json());
app.use(express.urlencoded({extended:false}));


app.post( '/login', (req,res)=>{
    let one = req.body.u;
    let two = req.body.p;
    
    fs.readFile('users.json', (err, data) => {
        let users = JSON.parse(data);
        for (let i in users) {
            if (users[i].u == one && users[i].p == two) {
               req.session.user = users[i].u;
                res.end('OK');
                return;
            } 
        }
        res.end('notOK');
    })
})


app.post('/register', (req,res) => {
    let one = req.body;
    fs.readFile('users.json', (err, data) => {
        let users = JSON.parse(data);
        users.push(one);
        fs.writeFile('users.json', JSON.stringify(users), (err) => {
            res.send(sending);
        });
    })

    res.end('OK');
})

app.post('/sendjson', (req,res) => {
    let reqObject = JSON.parse(req.body.songNames);
        fs.writeFile(__dirname +'/currentplaylist.json', JSON.stringify(reqObject), (err) => {
            if (err) {
                console.error(err)
                return
            }
        });
       
    res.end('OK');
})
