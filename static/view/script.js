const fs = require('fs');
const path = require('path');


// Load data from playlist on start

window.onload = (loadtunes) => {
  let oldcounter = 0;
  let output= '';
  var data = JSON.parse(fs.readFileSync('../server/currentplaylist.json'));
  for (i = 0; i < data.length; i++) {

    var result2 = /[^\\]*$/.exec(data[i].name);
    
    output += '<div class="folder draggable" draggable="true">' + result2 + '<button class="btn">X</button><button id="playy" data-id="' + (oldcounter++) + '">P</button></div>';


    songs.push(data[i].name);

    document.querySelector("#output").innerHTML = output;
   
    adddrag();

  }
};

// Songs array

let songs = [];
let aktuellerSong = -1;

// Drag and Drop

var dropArea = document.querySelector("#dropArea");

dropArea.addEventListener(
  "dragover",
  function (e) {
    e = e || event;
    e.preventDefault();
  },
  false
);

dropArea.addEventListener(
  "drop",

  function (event) {
    songs.length = 0;
    
    var items = event.dataTransfer.files;
    event.preventDefault();

    getFilesDataTransferItems(items).then(files => {

      let output = '';
      let songlist = [];
      let counter = 0;
      let counterdelete = 0;

      for (let i in files) {

        var str = files[i];
        var n = str.lastIndexOf("\\");
        var result2 = /[^\\]*$/.exec(files[i])[0];

        if (path.extname(files[i]) == ".mp3") {

          output += '<div class="folder draggable" draggable="true">' + result2 + '<button class="btn" data-id="' + (counterdelete++) + '" >X</button><button id="playy" data-id="' + (counter++) + '">P</button></div>';

          songs.push(files[i]);

          songlist.push({
            id: i,
            name: items[i].path
          });


          document.querySelector("#output").innerHTML = output;
          adddrag();

          
        }
      }
      
      sendSongNameToServer(songlist);
    });
  },
  false
);


// Send song names to Server
function sendSongNameToServer(songNames) {
  $.ajax({
    url: 'http://localhost:9999/sendjson',
    method: 'post',
    data: { songNames: JSON.stringify(songNames) },
    success: resp => {

      if (resp === 'OK') {
        
      } else {
        console.log('Send does not work');
      }
    },
    error: () => {
      console.log('ERROR, Server connection');
    }
  })
}


// Folder drop

function getFilesDataTransferItems(dataTransferItems) {

  console.log(dataTransferItems)
  let songlist = [];
  let output = '';
  let patch = '\\';
  let files = [];
  
  return new Promise((resolve, reject) => {
    let entriesPromises = [];
    for (let it of dataTransferItems) {

      fs.readdir(it.path,
        { withFileTypes: true },
        (err, files) => {

          if (err)
            console.log('not a folder');
          else {
            let zweitecounter = 0;
            let counterdelete = 0;
            files.forEach((file, id) => {

              // For showing only .mp3 files
              if (path.extname(file.name) == ".mp3") {

                output += '<div class="folder draggable" draggable="true">' + file.name + '<button class="btn" data-id="' + counterdelete++ + '">X</button><button id="playy" data-id="' + zweitecounter++ + '">P</button></div>';

                document.querySelector("#output").innerHTML = output;
                songs.push(it.path + patch + file.name);
            

                songlist.push({
                  id: zweitecounter,
                  name: it.path + patch + file.name
                });

                sendSongNameToServer(songlist);

                adddrag();
              }
            })
          }
        })
      files.push(it.path);
      resolve(files);
    }
  });
}

// Delete Button
$(document).on('click', '.btn', function () {
  var cur = $(this);
  cur.parent().remove();
 /* console.log(cur.data("id"));
  
  songs.splice(cur.data("id"), 1);
  console.log(songs);*/
 
});

// Play Audio
let pathorig;
let pathsl;
let audio;
let currentsong;

const playAudio = (id) => {
  if (audio) {
    audio.pause();
  }
  pathorig = songs[id];
  aktuellerSong = id;
  pathsl = pathorig /*.slice(0, -3);*/
  audio = new Audio(pathsl);
  audio.volume = 0.1;
  audio.play();
  audio.addEventListener('timeupdate', updateTime);

  audio.onended = function () {
    if (shufflecounter === 0) {
      aktuellerSong++;
      playAudio(aktuellerSong);
    } else if (shufflecounter === 1) {

      let songstotal = songs.length;
      randomnumber = Math.round(getRandomArbitrary(0, songstotal));
      if (randomnumber === aktuellerSong) {
        let newrandom = Math.round(getRandomArbitrary(0, songstotal));
        playAudio(newrandom);
      } else {
        playAudio(randomnumber);
      }
    }
  }

  currentsong = /[^\\]*$/.exec(pathsl)[0];

  if (currentsong !== 'undefined') {

    document.querySelector("#songname").innerHTML = currentsong;
  } else {
    document.querySelector("#songname").innerHTML = 'End of the playlist';
  }
 

  // Update time

  function updateTime() {
    const parseTime = time => {
      const seconds = String(Math.floor(time % 60) || 0).padStart('2', '0');
      const minutes = String(Math.floor(time / 60) || 0).padStart('2', '0');

      return `${minutes}:${seconds}`

    };

    const { currentTime, duration } = audio;
    document.getElementById('timer').innerHTML = `${parseTime(currentTime)}/${parseTime(duration)}`;
  };
}

// Shuffle

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}


let shufflecounter = 0;
let randomnumber;

$(document).on('click', '#shufflesong', function () {
  if (shufflecounter === 0) {

    shufflecounter = 1;
    let songstotal = songs.length;
    randomnumber = Math.round(getRandomArbitrary(0, songstotal -1 ));
    if (randomnumber === aktuellerSong) {
      let newrandom = Math.round(getRandomArbitrary(0, songstotal -1 ));
      playAudio(newrandom);
    } else {
      playAudio(randomnumber);
    }

    document.getElementById('shufflediv').innerHTML = 'Shuffle is on'
  } else if (shufflecounter === 1) {
    shufflecounter = 0;
    document.getElementById('shufflediv').innerHTML = '';
  }
})

// Next song
$(document).on('click', '#nextSong', function () {
  if (shufflecounter === 0) {
    aktuellerSong++;
    playAudio(aktuellerSong);
  } else if (shufflecounter === 1) {

    let songstotal = songs.length;
    randomnumber = Math.round(getRandomArbitrary(0, songstotal -1 ));
    if (randomnumber === aktuellerSong) {
      let newrandom = Math.round(getRandomArbitrary(0, songstotal -1));
      playAudio(newrandom);
    } else {
      playAudio(randomnumber);
    }
  }
})

// Last song 

$(document).on('click', '#lastsong', function () {
  if (shufflecounter === 0) {
    aktuellerSong--;
    playAudio(aktuellerSong);
  } else if (shufflecounter === 1) {

    let songstotal = songs.length;
    randomnumber = Math.round(getRandomArbitrary(0, songstotal -1 ));
    if (randomnumber === aktuellerSong) {
      let newrandom = Math.round(getRandomArbitrary(0, songstotal -1));
      playAudio(newrandom);
    } else {
      playAudio(randomnumber);
    }
  }
})


$(document).on('click', '#playy', function () {

  var id = $(this).attr('data-id');
 
  
  playAudio(id);
});

// Change volume
$(document).on('click', '#up', function () {
  audio.volume += 0.1;
})

$(document).on('click', '#down', function () {
  audio.volume -= 0.1;
})


// Pause audio

$(document).on('click', '#pausebtn', function () {
  audio.pause();
});

// Resume audio

$(document).on('click', '#playbtn', function () {
  audio.play();
});

// Playlist drag drop
function adddrag() {
  const draggables = document.querySelectorAll('.draggable')
  draggables.forEach(draggable => {
    
    draggable.addEventListener('dragstart', () => {
      draggable.classList.add('dragging')

    })
    draggable.addEventListener('dragend', () => {
      draggable.classList.remove('dragging')

    })
  })
}


// Moving playlist elements
const containers = document.querySelectorAll('.container')

containers.forEach(container => {
  container.addEventListener('dragover', e => {
    e.preventDefault()
    const afterElement = getDragAfterElement(container, e.clientY)
    const draggable = document.querySelector('.dragging')
    if (afterElement == null) {
      container.appendChild(draggable)
    } else {
      
      container.insertBefore(draggable, afterElement)
    }
  })
})

function getDragAfterElement(container, y) {
  const draggableElements = [...container.querySelectorAll('.draggable:not(.dragging)')]

  return draggableElements.reduce((closest, child) => {
    const box = child.getBoundingClientRect()
    const offset = y - box.top - box.height / 2
    if (offset < 0 && offset > closest.offset) {
      return { offset: offset, element: child }
    } else {
      return closest
    }
  }, { offset: Number.NEGATIVE_INFINITY }).element
}