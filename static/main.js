const { app, BrowserWindow} = require('electron');
let meinFenster;

const starteApplikation = () => {
    meinFenster = new BrowserWindow({
        width: 1400,
        height: 900,
        x: 0,
        y: 0,
        resizable:true,
        movable: true,       
        frame: true,
        icon: __dirname + '/assets/music.png',
        webPreferences: {
            nodeIntegration: true, 
            contextIsolation: false,
            devTools: true
        },
    });
    meinFenster.loadFile('./view/index.html');
    meinFenster.webContents.openDevTools(); 
   
};

app.on('ready', starteApplikation);

