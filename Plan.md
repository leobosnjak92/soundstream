Summary

-- Must:

- Login
- Registration
- Artist catalogue
- Ability to play/pause/stop songs.
- Showing the current name of the song
- Option to add a song to the playlist
- Playlist category in which user has an overview of the current playlist
- Ability to start playing a playlist from the beginning
- Logout

-- Should

- Every artist has an Avatar
- Volume level adjustment
- The music player should always be on
- "Shuffle play" option in playlist
- Ability to create multiple playlists and manipulate them

-- Nice

- Ability to share and access playlists with and from other users

-- Not

??

---- MILESTONES


## C2 18.2
-HTML/CSS for all the pages  
-Responsive website  
-Login/Registration  
-Create a database with songs

## C3 4.3
-Pulling the song data from database  
-Avatar for artists in database, and pulling it from the database  
-Functional player  
-Adding songs to a playlist and displaying it in the list


## C4 18.3
-Play and shuffle option in playlist  
-Multiple playlists  
-Playlist editing  
-Volume level adjustment  
-Logout


## C5 1.4
- Share and access to playlists of different users  
- Error tested



